package maja.spa;

import junit.framework.TestCase;
import maja.spa.BinarySearchTree;
import maja.spa.LinkedList;

public class PublicTests extends TestCase {
	private LinkedList list;
	private BinarySearchTree bst;

	protected void setUp() throws Exception {
		list = new LinkedList();
		list.insert(2);
		list.insert(5);
		list.insert(4);
		list.insert(1);
		list.insert(8);
		list.insert(3);

		bst = new BinarySearchTree();
		bst.insert(2);
		bst.insert(5);
		bst.insert(4);
		bst.insert(1);
		bst.insert(8);
		bst.insert(3);
	}

	public void testSeznamIskanjeUspesno() {
		assertTrue(list.search(3));
	}

	public void testSeznamIskanjeNeuspesno() {
		assertFalse(list.search(10));
	}

	public void testSeznamBrisanje() {
		list.delete(4);
		assertFalse(list.search(4));
	}

	public void testBinarnoIskanjeUspesno() {
		assertTrue(bst.search(3));
	}

	public void testBinarnoIskanjeNeuspesno() {
		assertFalse(bst.search(10));
	}

	public void testBinarnoBrisanje() {
		bst.delete(4);
		assertFalse(bst.search(4));
	}

}
