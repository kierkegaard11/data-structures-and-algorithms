package maja.spa;

public class BSTNode {

	private int key;
	private BSTNode left;
	private BSTNode right;

	public BSTNode(int key) {
		this.key = key;
	}

	public BSTNode() {
	}

	public BSTNode(int key, BSTNode left, BSTNode right) {
		this.key = key;
		this.left = left;
		this.right = right;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public BSTNode getLeft() {
		return left;
	}

	public void setLeft(BSTNode left) {
		this.left = left;
	}

	public BSTNode getRight() {
		return right;
	}

	public void setRight(BSTNode right) {
		this.right = right;
	}

	public int compare(BSTNode node) {
		return node.key - this.key;
	}

	public boolean search(int value) {
		BSTNode node = new BSTNode(value);
		if (this.compare(node) == 0)
			return true;
		else if (this.compare(node) < 0) {
			if (left == null)
				return false;
			else
				return left.search(value);
		} else if (this.compare(node) > 0) {
			if (right == null)
				return false;
			else
				return right.search(value);
		}
		return false;
	}

	public boolean insert(int value) {
		BSTNode node = new BSTNode(value);
		if (this.compare(node) == 0)
			return false;
		else if (this.compare(node) < 0) {
			if (left == null) {
				left = new BSTNode(value);
				return true;
			} else
				return left.insert(value);
		} else if (this.compare(node) > 0) {
			if (right == null) {
				right = new BSTNode(value);
				return true;
			} else
				return right.insert(value);
		}
		return false;
	}

	public boolean remove(int value, BSTNode parent) {
		BSTNode node = new BSTNode(value);
		if (this.compare(node) < 0) {
			if (left != null)
				return left.remove(value, this);
			else
				return false;
		} else if (this.compare(node) > 0) {
			if (right != null)
				return right.remove(value, this);
			else
				return false;
		} else {
			if (left != null && right != null) {
				this.key = right.minValue();
				right.remove(this.key, this);
			} else if (parent.left == this) {
				parent.left = (left != null) ? left : right;
			} else if (parent.right == this) {
				parent.right = (left != null) ? left : right;
			}
			return true;
		}

	}

	public int minValue() {
		if (left == null)
			return key;
		else
			return left.minValue();
	}

}
