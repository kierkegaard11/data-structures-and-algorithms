package maja.spa;

public class LinkedListNode {
	private int key;
	private LinkedListNode next;

	public LinkedListNode(int key, LinkedListNode next) {
		this.key = key;
		this.next = next;
	}

	public LinkedListNode(int key) {
		this.key = key;
	}

	public LinkedListNode() {
	}

	public void insert(LinkedListNode node) {
		this.next = node;
	}

	public void delete(LinkedListNode node) {
		this.next = node.next;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public LinkedListNode getNext() {
		return next;
	}

	public void setNext(LinkedListNode next) {

		this.next = next;
	}

	public int compare(LinkedListNode node) {
		return node.key - this.key;
	}

}
