package maja.spa;

public class BinarySearchTree {

	private BSTNode root;

	public BinarySearchTree(BSTNode root) {
		this.root = root;
	}

	public BSTNode getRoot() {
		return root;
	}

	public BinarySearchTree() {
		root = null;
	}

	public boolean insert(int element) {
		if (root == null) {
			root = new BSTNode(element);
			return true;
		}
		return root.insert(element);
	}

	public boolean search(int element) {
		return root.search(element);
	}

	public boolean delete(int value) {
		if (root == null)
			return false;
		else {
			if (root.compare(new BSTNode(value)) == 0) {
				BSTNode tmp = new BSTNode(0);
				tmp.setLeft(root);
				boolean result = root.remove(value, tmp);
				root = tmp.getLeft();
				return result;
			} else {
				return root.remove(value, null);
			}
		}
	}
}
