package maja.spa;

public class LinkedList {

	private LinkedListNode head;

	public LinkedList() {
	}

	public boolean insert(int element) {
		LinkedListNode newNode = new LinkedListNode(element);
		if (head == null) {
			head = newNode;
			return true;
		}
		LinkedListNode tmp = head;
		while (tmp.getNext() != null) {
			if (tmp.compare(newNode) == 0)
				return false;
			tmp = tmp.getNext();
		}
		tmp.insert(newNode);

		return true;
	}

	public boolean delete(int element) {
		if (head == null)
			return false;
		LinkedListNode nodeToDelete = new LinkedListNode(element);
		if (head.compare(nodeToDelete) == 0) {
			head = head.getNext();
			return true;
		}
		LinkedListNode tmp = head;
		while (tmp.getNext() != null && tmp.getNext().compare(nodeToDelete) != 0) {
			tmp = tmp.getNext();
		}
		if (tmp.getNext() == null)
			return false;
		tmp.delete(tmp.getNext());
		return true;
	}

	public boolean search(int element) {
		if (head == null)
			return false;
		LinkedListNode tmp = head;
		while (tmp != null) {
			if (tmp.compare(new LinkedListNode(element)) == 0) {
				return true;
			}
			tmp = tmp.getNext();
		}
		return false;
	}

}
